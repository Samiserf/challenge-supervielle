# Challenge-supervielle

## Paginas

/ -> En esta pagina se encuentra toda la logica del challenge y redirige a "/" al completar el formulario y tocar el boton "ingresar"
/home -> Simulacion de home, solo tiene una imagen tal cual el challenge.

## Tecnologias

- [ ] React.
- [ ] Sass & clsx.
- [ ] TypeScript.
- [ ] react-router-dom. -> Se utilizo para el routeo de la web
- [ ] Jest & testing-library.
- [ ] StoryBook. -> Se utilizo storyBook para el testing de componentes atomos. (Text, Input, Button)
- [ ] Formik. -> Se utilizo formik para la integracion del formulario en la pagina "login"
- [ ] Yup. -> Se utilizo Yup para las validaciones del formurio.. Sobre la misma libreria se utilizo Regex para validaciones mas complejas

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] Para ejecutar el proyecto localmente -> npm start
- [ ] Para ejecutar storybook -> npm run storybook
- [ ] Para ejecutar los tests -> npm run test

***