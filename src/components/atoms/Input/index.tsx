import { forwardRef, InputHTMLAttributes, useState } from "react";
import clsx from "clsx";
import styles from "./styles.module.scss";

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  /** Define component props here */
  inputSize?: "md" | "s";
  invalid?: boolean;
  messageInvalid?: string;
  icon?: boolean;
  className?: string;
  label?: string;
}

export const Input = forwardRef<HTMLInputElement, InputProps>(
  (
    {
      type = "text",
      invalid = false,
      messageInvalid = "",
      icon = false,
      inputSize = "md",
      className,
      value,
      label,
      onChange,
      ...rest
    },
    ref
  ) => {
    const [currentType, setCurrentType] = useState(type);

    const props = {
      type,
      className: clsx(
        [styles["content-input-main"]],
        invalid && [styles["content-input-error"]],
        inputSize === "s" && [styles["content-input-small"]],
        inputSize === "md" && [styles["content-input-medium"]],
        className
      ),
      [onChange ? "value" : "defaultValue"]: value,
      onChange,
    };

    return (
      <div className={clsx([styles["content"]])}>
        {label && (
          <label className={clsx([styles["content-label"]])}>{label}</label>
        )}
        <div className={clsx([styles["content-input"]])}>
          <input
            {...props}
            type={type === "password" ? currentType : type}
            {...rest}
            ref={ref}
          />
          {icon && (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className={clsx([styles["content-input-svg"]])}
              viewBox="0 0 20 20"
              fill="currentColor"
              onClick={() =>
                setCurrentType(currentType === "password" ? "text" : "password")
              }
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M2.97583 2.59836C4.77023 1.08128 7.25104 9.91821e-05 10.0023 9.91821e-05C12.7529 9.91821e-05 15.2337 1.08044 17.0283 2.59716C18.8033 4.09736 20.0043 6.11414 20.0043 8.0521C20.0043 9.99006 18.8033 12.0068 17.0283 13.507C15.2337 15.0238 12.7529 16.1041 10.0023 16.1041C7.25104 16.1041 4.77023 15.0229 2.97583 13.5058C1.20105 12.0053 0.000305176 9.98859 0.000305176 8.0521C0.000305176 6.11561 1.20105 4.09886 2.97583 2.59836ZM3.94428 3.74384C2.37056 5.07434 1.50031 6.70859 1.50031 8.0521C1.50031 9.39561 2.37056 11.0299 3.94428 12.3604C5.49838 13.6743 7.64357 14.6041 10.0023 14.6041C12.3607 14.6041 14.5059 13.6749 16.0601 12.3614C17.6338 11.0314 18.5043 9.39714 18.5043 8.0521C18.5043 6.70706 17.6338 5.07284 16.0601 3.74279C14.5059 2.42926 12.3607 1.5001 10.0023 1.5001C7.64357 1.5001 5.49838 2.42992 3.94428 3.74384ZM10.0023 5.6401C8.67036 5.6401 7.59031 6.71947 7.59031 8.0521C7.59031 9.38389 8.67052 10.4641 10.0023 10.4641C11.3341 10.4641 12.4143 9.38389 12.4143 8.0521C12.4143 6.71947 11.3342 5.6401 10.0023 5.6401ZM6.09031 8.0521C6.09031 5.89073 7.84225 4.1401 10.0023 4.1401C12.1624 4.1401 13.9143 5.89073 13.9143 8.0521C13.9143 10.2123 12.1625 11.9641 10.0023 11.9641C7.84209 11.9641 6.09031 10.2123 6.09031 8.0521Z"
                fill="#3B3A45"
              />
            </svg>
          )}
        </div>
        <p className={clsx([styles["content-invalid"]])}>{messageInvalid}</p>
      </div>
    );
  }
);

Input.displayName = "Input";

export default Input;
