# Input component 🧩

Simple input component supporting both controlled and uncontrolled functionality (with onChange prop), focus, alert and disabled styles and medium and small size variants.
