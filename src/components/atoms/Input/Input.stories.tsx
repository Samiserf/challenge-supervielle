import { Meta, Story } from '@storybook/react';
import { Input, InputProps } from './index';
import { useArgs } from '@storybook/client-api';

const meta: Meta = {
  title: 'Atoms/Input',
  component: Input,
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const ControlledTemplate: Story<InputProps> = (args) => {
  const [{ value }, updateArgs]: [
    InputProps,
    (newArgs: Partial<InputProps>) => void,
    unknown
  ] = useArgs();

  return (
    <Input
      {...args}
      value={value}
      name="prueba"
      onChange={(e) => updateArgs({ value: e.target.value })}
    />
  );
};
const Template: Story<InputProps> = (args) => <Input {...args} />;

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing

export const Controlled = ControlledTemplate.bind({});

Controlled.args = {
  value: 'current value',
  disabled: false,
  invalid: false,
  type: 'value',
  inputSize: 'md',
  placeholder: 'This is the placeholder',
  onChange: () => {
    return null;
  },
  label: "prueba",
  name: "prueba"
};

export const Uncontrolled = Template.bind({});

Uncontrolled.args = {
  value: 'initial value',
  disabled: false,
  invalid: false,
  type: 'value',
  inputSize: 'md',
  placeholder: 'This is the placeholder',
  label: "prueba",
  name: "prueba"
};
