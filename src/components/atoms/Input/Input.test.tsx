import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';

import { Uncontrolled as Input } from './Input.stories';
import { ChangeEvent } from 'react';

describe('Input', () => {
  describe('Render', () => {
    it('Using default props', () => {
      const { container } = render(<Input name="prueba"/>);
      expect(container).toMatchSnapshot();
    });

    it('Using type prop', () => {
      const { container } = render(<Input name="prueba" type="number" />);
      expect(container).toMatchSnapshot();
    });

    it('Using value prop', () => {
      const { container } = render(<Input name="prueba" value="value" />);
      expect(container).toMatchSnapshot();
    });

    it('Using value with onChange', () => {
      const onChange = jest.fn();
      const { container } = render(<Input name="prueba" value="value" onChange={onChange} />);
      expect(container).toMatchSnapshot();
    });

    it('Using placeholder prop', () => {
      const { container } = render(<Input name="prueba" placeholder="placeholder" />);
      expect(container).toMatchSnapshot();
    });

    it('Using disabled prop', () => {
      const { container } = render(<Input name="prueba" disabled />);
      expect(container).toMatchSnapshot();
    });

    it('Using invalid prop', () => {
      const { container } = render(<Input name="prueba" invalid />);
      expect(container).toMatchSnapshot();
    });
  });

  describe('Callback', () => {
    it('Triggering onChange prop', async () => {
      const user = userEvent.setup();
      const _onChange = jest.fn();
      const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        _onChange(e.target.value);
      };
      render(<Input data-testid="input" name="prueba" value={''} onChange={onChange} />);
      expect(_onChange).not.toHaveBeenCalled();
      await user.type(screen.getByRole('textbox'), 'x');
      expect(_onChange).toBeCalledWith('x');

      await user.type(screen.getByRole('textbox'), 'y');
      expect(_onChange).toBeCalledWith('y');
    });

    it('Triggering onBlur prop', async () => {
      const user = userEvent.setup();
      const onBlur = jest.fn();
      render(<Input data-testid="input" name="prueba" onBlur={onBlur} />);

      expect(onBlur).not.toBeCalled();

      await user.click(screen.getByRole('textbox'));
      await user.keyboard('[Tab]');
      expect(onBlur).toHaveBeenCalledTimes(1);

      await user.click(screen.getByRole('textbox'));
      await user.click(document.body);
      expect(onBlur).toHaveBeenCalledTimes(2);
    });
  });
});
