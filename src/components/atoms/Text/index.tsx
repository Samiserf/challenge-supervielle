import { forwardRef, ReactNode, ElementType } from 'react';
import clsx from 'clsx';
import { WeightValues, SizeValues } from '../../../utils/types';
import styles from './styles.module.scss';

const enum As {
  Paragraph = 'paragraph',
  Span = 'span',
  Em = 'em',
  Strong = 'strong',
  Underline = 'underline',
}

export interface TextProps {
  as?: `${As}`;
  size?: SizeValues;
  bold?: WeightValues;
  className?: string | { [key: string]: boolean } | undefined;
  children: ReactNode | undefined | JSX.Element;
}

const TAGS: { [key in `${As}`]: ElementType } = {
  [As.Paragraph]: 'p',
  [As.Span]: 'span',
  [As.Em]: 'em',
  [As.Strong]: 'strong',
  [As.Underline]: 'u',
};

export const Text = forwardRef<HTMLParagraphElement, TextProps>(
  (
    {
      as = `${As.Paragraph}`,
      size = 'md',
      bold = as === 'strong' ? 600 : 400,
      children,
      className,
      ...props
    },
    ref
  ) => {
    const style = clsx([
      styles.text,
      styles[`text--${as}`],
      styles[`font-size-${size}`],
      styles[`line-height-size-${size}`],
      styles[`font-weight-${bold}`],
      className,
    ]);

    const Tag = TAGS[as];
    return (
      <Tag className={style} {...props} ref={ref}>
        {children}
      </Tag>
    );
  }
);
Text.displayName = 'Text';
export default Text;
