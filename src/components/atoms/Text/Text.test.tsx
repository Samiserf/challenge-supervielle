import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { Overview as Text } from './Text.stories';

describe('Text', () => {
  it('should render Text', () => {
    render(
      <Text as="paragraph" data-testid="exist">
        Test
      </Text>
    );
    const text = screen.getByTestId('exist');
    expect(text).toBeInTheDocument();
    expect(text).toMatchSnapshot();
  });

  it('should be bold', () => {
    render(
      <Text bold={600} data-testid="bold">
        bold text test
      </Text>
    );
    expect(screen.getByTestId('bold')).toHaveClass('font-weight-600');
  });

  it('should change size', () => {
    render(
      <Text size="xl" data-testid="size">
        size text test
      </Text>
    );
    expect(screen.getByTestId('size')).toHaveClass('font-size-xl');
    expect(screen.getByTestId('size')).toHaveClass('line-height-size-xl');
  });

  it('should be strong and bold', () => {
    render(<Text as="strong">strong text test</Text>);
    expect(screen.getByText('strong text test')).toBeInTheDocument();
    expect(screen.getByText('strong text test')).toHaveClass('font-weight-600');
  });

  it('have custom classes', () => {
    render(
      <Text className="custom" data-testid="customClass">
        class text test
      </Text>
    );
    expect(screen.getByTestId('customClass')).toHaveClass('custom');
  });
});
