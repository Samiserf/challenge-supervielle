import { Meta, Story } from '@storybook/react';
import Text, { TextProps } from './index';

const meta: Meta = {
  title: 'Atoms/Text',
  component: Text,
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story<TextProps> = (args) => <Text {...args} />;

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing

export const Overview = Template.bind({});

Overview.args = {
  children: 'Text example test',
};
