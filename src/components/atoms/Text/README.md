# Text component 🧩

## What variants does it have?

1.  **Variant 1 as:** 'paragraph' | 'span' | 'em' | 'strong' | 'underline' <br>
    **Variant 2 size:**
    | '2xs'
    | 'xs'
    | 's'
    | 'md'
    | 'l'
    | 'xl'
    | '2xl'
    | '3xl'
    | '4xl'
    | '5xl'
    | '6xl'
    <br>
    **Variant 4 bold:**
    accept numbers (every 100, from 100 to 900):<br>
    | 100
    | 200
    | 300
    | 400
    | 500
    | 600
    | 700
    | 800
    | 900 <br>
    **Variant 5 classNames:** to set custom className, just pass className="nameClass"

## Default values

as="p", size="l", bold="normal"

## Defining a ref value

component accept ref as a prop. Define the ref variable and pass it as a prop.

## Common usage

`<Text as="s" size="s" ref={refExample} className="test" bold={600}>Test</Text>`<br> returns: <br>`<span className="test" style={{fontSize: 0.875rem, fontWeight: 600, line-height: 1.25rem }}>test span</span>`
