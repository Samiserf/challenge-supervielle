import { forwardRef, HTMLAttributes, RefObject } from "react";
import clsx from "clsx";
import Text from "../Text/index";
import styles from "./styles.module.scss";

export interface ButtonPropsBasics
  extends Omit<HTMLAttributes<any>, "className"> {
  variant?: "primary" | "secondary" | "link";
  size?: "xs" | "s" | "md" | "l";
  as?: "a" | "button";
  icon?: React.ElementType | undefined | React.FunctionComponent<any>;
  loadingMessage?: string;
  iconPosition?: "left" | "right";
  fullWidth?: boolean;
  isLoading?: boolean;
  children: React.ReactNode | JSX.Element;
}

export type ButtonProps = ButtonPropsBasics &
  (
    | (JSX.IntrinsicElements["a"] & { onClick?: never })
    | (JSX.IntrinsicElements["button"] & { href?: never })
  );

const LoadingIcon = (
  <svg
    className={clsx([
      styles["container-button-icon-spinner"],
      styles["container-button-icon"],
    ])}
    width="28"
    height="28"
    viewBox="0 0 28 28"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M22.4 13.9998C22.4 9.36061 18.6392 5.5998 14 5.5998C9.36085 5.5998 5.60004 9.36061 5.60004 13.9998C5.60004 18.639 9.36085 22.3998 14 22.3998C14.7732 22.3998 15.4 23.0266 15.4 23.7998C15.4 24.573 14.7732 25.1998 14 25.1998C7.81445 25.1998 2.80004 20.1854 2.80004 13.9998C2.80004 7.81421 7.81445 2.7998 14 2.7998C20.1856 2.7998 25.2 7.81422 25.2 13.9998C25.2 14.773 24.5732 15.3998 23.8 15.3998C23.0268 15.3998 22.4 14.773 22.4 13.9998Z"
    />
  </svg>
);

/**
 * Button component.
 *
 *
 * @param variant {String} { 'primary' | 'secondary' | 'alternative-blue' | 'alternative-white' | 'alternative-gray' | 'hero' | 'boolean'}
 * @param icon a react element to show icon. Send SVG as component.
 * @param iconPosition {String} {'left' | 'right'}
 * @param size {String }{'xs' | 's' | 'md' | 'l'}
 * @param isLoadingize {Boolean} when true, loading svg and animation should trigger.
 * @param loadingMessage {String} when isLoading is true, replace children by sending a custom message.
 * @param fullWidth {Boolean} to fit 100% parent´s container.
 * @param as {String} {'as' | 'button'}.
 */

type Ref =
  | RefObject<HTMLAnchorElement>
  | HTMLAnchorElement
  | RefObject<HTMLButtonElement>
  | HTMLButtonElement
  | null;

export const Button = forwardRef<Ref, ButtonProps>(
  (
    {
      className,
      variant = "primary",
      icon: Icon,
      iconPosition,
      size = "xs",
      isLoading,
      loadingMessage = "Ingresando...",
      fullWidth = false,
      as = "button",
      children,
      ...props
    },
    ref
  ) => {
    const style = clsx([
      styles["container-button"],
      styles[`container-button-${variant}`],
      styles[`container-button-size-${size}`],
      iconPosition && styles[`container-button-position-${iconPosition}`],
      fullWidth && styles["container-button-full-width"],
      isLoading && variant === "primary" && styles["container-button-loading"],
      className,
    ]);

    const Component = as || "button";

    return (
      <Component tabIndex={0} className={style} ref={ref} {...(props as any)}>
        <Text
          as="span"
          className={clsx([styles["container-button-text"]])}
          bold={700}
          size={size === "xs" ? "s" : size}
        >
          {isLoading ? <Text>{loadingMessage}</Text> : children}
        </Text>
        {Icon && (
          <>
            {isLoading && variant === "primary" ? (
              LoadingIcon
            ) : (
              <Icon className={clsx([styles["container-button-icon"]])} />
            )}
          </>
        )}
        {isLoading && variant === "primary" && (
          <div className={clsx([styles["container-button-progress-bar"]])}>
            &nbsp;
          </div>
        )}
      </Component>
    );
  }
);

Button.displayName = "Button";
export default Button;
