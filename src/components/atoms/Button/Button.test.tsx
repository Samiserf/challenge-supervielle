import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import './styles.module.scss';
import { Overview as Button } from './Button.stories';

const Icon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-6 w-6 container-button-icon-icon"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    width="20"
    height="20"
    data-testid="icon"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      className="container-button-icon-icon"
      d="M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z"
    />
  </svg>
);

const myFn = jest.fn();

describe('Button', () => {
  it('should render Button', () => {
    render(<Button>My button</Button>);

    const component = screen.getByRole('button');
    expect(component).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('should have variant class', () => {
    render(<Button variant="secondary">My button</Button>);

    const component = screen.getByRole('button');
    expect(component).toHaveClass('container-button-secondary');
    expect(component).toMatchSnapshot();
  });

  it('should have icon', () => {
    render(<Button icon={Icon}>My button</Button>);

    const component = screen.getByTestId('icon');
    expect(component).toBeInTheDocument();
    expect(component).toHaveClass('container-button-icon-icon');
    expect(component).toMatchSnapshot();
  });

  it('should be clickeable', () => {
    render(<Button onClick={() => myFn()}>My button</Button>);

    const component = screen.getByRole('button');
    fireEvent.click(component);

    expect(myFn).toHaveBeenCalled();
    expect(component).toMatchSnapshot();
  });

  it('should have 100% of width', () => {
    render(<Button fullWidth>My button</Button>);
    const component = screen.getByRole('button');
    expect(component).toHaveClass('container-button-full-width');
    expect(component).toMatchSnapshot();
  });

  it('should render anchor tag', () => {
    render(
      <Button as="a" href="test">
        My button
      </Button>
    );
    const component = screen.getByRole('link');
    expect(component).toHaveAttribute('href', 'test');
    expect(component).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });
});
