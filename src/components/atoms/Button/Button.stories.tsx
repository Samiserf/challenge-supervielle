import { Meta, Story } from '@storybook/react';
import Button, { ButtonProps } from './index';

const meta: Meta = {
  title: 'Atoms/Button',
  component: Button,
  parameters: {
    controls: { expanded: true },
  },
  argTypes: {
    icon: {
      table: {
        disable: true,
      },
    },
  },
};

export default meta;

const Template: Story<ButtonProps> = (args: any) => (
  <Button {...args}>My Button</Button>
);

export const Overview = Template.bind({});

Overview.args = {};

const TemplateAnchor: Story<ButtonProps> = (args: any) => (
  <Button style={{ display: 'inline-block' }} {...args}>
    My anchor button
  </Button>
);

export const AnchorButton = TemplateAnchor.bind({});

AnchorButton.args = { as: 'a', href: 'http://www.karvi.com', target: '_blank' };

export const WithIcon = Template.bind({});

const Icon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="h-6 w-6 container-button-icon-icon"
    fill="none"
    viewBox="0 0 24 24"
    stroke="currentColor"
    width="20"
    height="20"
  >
    <path
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      className="container-button-icon-icon"
      d="M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z"
    />
  </svg>
);

WithIcon.args = {
  children: 'With Icon',
  icon: Icon,
};
