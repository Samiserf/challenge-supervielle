# Button component 🧩

> Create a custom button component

## What variants does it have?

1. Hover
2. Active
3. Focus

## What states does it have?

1. size: Swich between this sizes: xs, s, md, l.
2. className: Receive a custom class
3. variant: "primary" | "secondary" | "alternative-blue" | "alternative-gray" | "alternative-white" | "hero" | "boolean"
4. isLoading: When is true and the button is primary, an animation will be triggered.
5. icon: send imported SVG/image
6. fullWidth: boolean
7. iconPosition: "right" or "left"
8. loadingMessage: Custom the laoding message
9. as: "a" || "button". "button" is the default value. Send `as="a"` to have an anchor semantic tag.
