import { BrowserRouter, Routes, Route } from "react-router-dom";
import styles from "./styles.module.scss";
import Login from "./pages/login";
import Home from "./pages/home";

function App() {
  return (
    <div className={styles.containerPage}>
      <BrowserRouter>
        <Routes>
          <Route index element={<Login />} />
          <Route path="home" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
