import React from "react";
import styles from "./styles.module.scss";
import BackgroundHome from "../../assets/img/background-home.png";

const Home: React.FC = () => {
  return (
      <img alt="imagen home" className={styles.background} src={BackgroundHome} />
  );
};

export default Home;
