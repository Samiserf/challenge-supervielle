import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./styles.module.scss";
import { useFormik } from "formik";
import * as Yup from "yup";
import clsx from "clsx";
import Button from "../../components/atoms/Button";
import Input from "../../components/atoms/Input";
import Text from "../../components/atoms/Text";
import logospv from "../../assets/img/Logosupervielle-logo.png";
import justLogoSpv from "../../assets/img/logospv.png";

const Login: React.FC = () => {
  const navigate = useNavigate();

  const [simulateLogin, setSimulateLogin] = useState(false);

  const [width, setWidth] = useState(window.innerWidth);

  const breakpoint = 576;

  useEffect(() => {
    window.addEventListener("resize", () => setWidth(window.innerWidth));
  }, []);

  const formik = useFormik({
    initialValues: {
      passport: "",
      user: "",
      password: "",
    },
    onSubmit: () => {
      setSimulateLogin(true);
      // Simulation of a request to backend
      setTimeout(() => {
        navigate("/home");
        setSimulateLogin(false);
      }, 2000);
    },
    validationSchema: Yup.object({
      passport: Yup.string()
        .min(8, "El DNI debe tener el formato correspondiente")
        .max(8)
        .required("Este campo es requerido *"),
      user: Yup.string()
        .max(12, "El usuario acepta un maximo de 12 caracteres")
        .matches(
          /^[aA1-zZ10\s]+$/,
          "El usuario no puede tener caracteres especiales"
        )
        .required("Este campo es requerido *"),
      password: Yup.string()
        .max(6, "La contraseña acepta un maximo de 6 caracteres")
        .matches(
          /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{3,6}$/,
          "La contraseña debe tener al menos 1 mayuscula y 1 nùmero."
        )
        .required("Este campo es requerido *"),
    }),
  });

  return (
    <div className={styles.login}>
      <h1>
        <img
          className={clsx([styles["login-logo"]])}
          src={width > breakpoint ? logospv : justLogoSpv}
          alt="Logo supervielle"
        />
      </h1>
      <Text
        as="span"
        size="xl"
        bold={500}
        className={clsx([styles["login-sign"]])}
      >
        Iniciar sesion
      </Text>
      <form
        onSubmit={formik.handleSubmit}
        className={clsx([styles["login-form"]])}
      >
        <Input
          inputSize="s"
          invalid={!!formik.errors.passport}
          messageInvalid={formik.errors.passport}
          onChange={(e) => formik.setFieldValue("passport", e.target.value)}
          name="passport"
          type="number"
          label="Documento"
          className={clsx([styles["login-form-input"]])}
        />
        <Input
          inputSize="s"
          onChange={(e) => formik.setFieldValue("user", e.target.value)}
          invalid={!!formik.errors.user}
          messageInvalid={formik.errors.user}
          name="user"
          type="text"
          label="Usuario"
          className={clsx([styles["login-form-input"]])}
        />
        <Input
          inputSize="s"
          onChange={(e) => formik.setFieldValue("password", e.target.value)}
          invalid={!!formik.errors.password}
          messageInvalid={formik.errors.password}
          icon
          type={"password"}
          name="password"
          label="Clave"
          className={clsx([
            styles["login-form-input"],
            styles["login-form-input-last"],
          ])}
        />
        <Button
          as="a"
          variant="link"
          className={clsx([styles["login-form-actions-link"]])}
        >
          <Text>¿Olvidaste tu usuario o clave?</Text>
        </Button>
        <div className={clsx([styles["login-form-actions"]])}>
          <Button
            type="submit"
            className={clsx([styles["login-form-actions-button"]])}
            isLoading={simulateLogin}
          >
            <Text>Ingresar</Text>
          </Button>
          <Button
            type="button"
            onChange={() => console.log("registrar")}
            className={clsx([styles["login-form-actions-button"]])}
            variant="secondary"
          >
            <Text>Registrarme</Text>
          </Button>
        </div>
      </form>
    </div>
  );
};

export default Login;
