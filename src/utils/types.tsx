export type WeightValues = 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900;

export type Headinglevels =
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6;

export type SizeValues =
  | '2xs'
  | 'xs'
  | 's'
  | 'md'
  | 'l'
  | 'xl'
  | '2xl'
  | '3xl'
  | '4xl'
  | '5xl'
  | '6xl';
